# Stone's Technical Test 

## Instalando as depêndencias

```bash
./gradlew
```

## Executar a aplicação

```bash
./gradlew run
```

## Rodar os testes

```bash
./gradlew test
```

## Build da aplicação

```bash
./gradlew build
```

Para executar a aplicação basta seguir os passos de instalar as depêndencias e executar a aplicação que estão listados logo acima, é necessário instalar dependências pois achei necessário a adição de uma para validar Email, uma vez que validação com regex pode não atender completamente as validações convencionadas na RFC 2822.

Os arquivos de código java se encontram na pasta:

`src/main/java/project`

Os arquivos de teste se encontram na pasta:

`src/test/java/project`
