package project;

import org.junit.Test;
import static org.junit.Assert.*;

public class ItemTest {
    @Test public void testItemGivesTheCorrectTotal() {
        Item item = new Item(3, 1500);

        assertEquals(4500, item.total());
    }
}
