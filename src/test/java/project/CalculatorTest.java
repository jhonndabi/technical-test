package project;

import java.util.List;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;

public class CalculatorTest {
    @Test public void testCalculator() {
        List<Item> itens = new ArrayList<Item>();
        itens.add(new Item(1, 45));
        itens.add(new Item(1, 5));
        itens.add(new Item(1, 35));
        itens.add(new Item(1, 10));
        itens.add(new Item(1, 4));
        itens.add(new Item(1, 1));

        List<Email> emails = new ArrayList<Email>();
        emails.add(new Email("contact@stone.com.br"));
        emails.add(new Email("me@stone.com.br"));
        emails.add(new Email("me@stone.com"));

        Calculator calculator = new Calculator(itens, emails);

        assertEquals(
            "{contact@stone.com.br=33, me@stone.com.br=33, me@stone.com=34}",
            calculator.calculate().toString()
        );
    }
}
