package project;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;

public class EmailTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test public void testEmailCreatedWithValidEmail() {
        Email email = new Email("test@test.com");

        assertTrue(email instanceof Email);
    }

    @Test public void testEmailIsNotCreatedWithInvalidEmail() throws IllegalArgumentException {
        String invalidEmail = "test@testcom";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("This email: '" + invalidEmail + "' is not valid.");

        Email email = new Email(invalidEmail);
    }
}
