package project;

import java.util.List;
import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        List<Item> itens = new ArrayList<Item>();
        itens.add(new Item(1, 45));
        itens.add(new Item(1, 5));
        itens.add(new Item(1, 35));
        itens.add(new Item(1, 10));
        itens.add(new Item(1, 4));
        itens.add(new Item(1, 1));

        List<Email> emails = new ArrayList<Email>();
        emails.add(new Email("contact@stone.com.br"));
        emails.add(new Email("me@stone.com.br"));
        emails.add(new Email("me@stone.com"));

        Calculator calculator = new Calculator(itens, emails);

        System.out.println(calculator.calculate());
    }
}
