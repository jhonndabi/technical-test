package project;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

class Calculator {
    private List<Item> items;
    private List<Email> emails;

    public Calculator(List<Item> items, List<Email> emails) {
        this.items = items;
        this.emails = emails;
    }

    public Map<String, Integer> calculate() {
        int totalOfEmails = emails.size();
        int itemsTotal = itemsTotal();

        Map<String, Integer> result = new HashMap<String, Integer>();

        for (int i = 0; i < totalOfEmails; i++) {
            Email email = emails.get(i);
            int total = (int) itemsTotal / (totalOfEmails - i);

            result.put(email.toString(), total);

            itemsTotal -= total;
        }

        return result;
    }

    private int itemsTotal() {
        int itemsTotal = 0; 

        for (Item item : items) {
            itemsTotal += item.total();
        }

        return itemsTotal;
    }
}
