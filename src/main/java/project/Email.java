package project;

import org.apache.commons.validator.routines.EmailValidator;

public class Email {
    private String email;

    public Email(String email) {
        if (!EmailValidator.getInstance().isValid(email)) {
            throw new IllegalArgumentException("This email: '" + email + "' is not valid.");
        }

        this.email = email;
    }

    public String toString() {
        return this.email;
    }
}
