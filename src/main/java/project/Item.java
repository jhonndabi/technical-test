package project;

public class Item {
    private int quantity;
    private int price;

    public Item(int quantity, int price) {
        this.quantity = quantity;
        this.price = price;
    }

    public int total() {
        return this.quantity * this.price;
    }
}
